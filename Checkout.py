from collections import Counter

class Checkout:

    def __init__(self, config):
        self.price_config = config
        self.total = 0
        self.grouped_basket = Counter()

    def do_checkout(self, basket):
        self.grouped_basket = self.__group_items(basket)
        self.__recalculate_total()
        return self.total

    def __group_items(self, basket):
        grouped = Counter()

        for item in basket:
            grouped[item] += 1

        return grouped

    def __get_subtotal_for_product(self, product_quantity, product_config):
        normal_price = product_config['price']

        if product_config.__contains__('special_quantity'):
            special_quantity = product_config['special_quantity']
            special_price = product_config['special_price']

            product_total = (product_quantity // special_quantity) * special_price
            product_total += (product_quantity % special_quantity) * normal_price
        else:
            product_total = product_quantity * normal_price

        return product_total

    def scan(self, product_name):
        self.grouped_basket[product_name] += 1
        self.__recalculate_total()

    def __recalculate_total(self):
        self.total = 0
        for item_name in self.grouped_basket:
            item_quantity = self.grouped_basket[item_name]
            item_config = self.price_config[item_name]
            self.total +=  self.__get_subtotal_for_product(item_quantity, item_config)
