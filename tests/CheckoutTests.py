from unittest import TestCase

from Checkout import Checkout


class CheckoutTests(TestCase):

    def setUp(self):
        self.checkout_test_config = {
            'apple': {
                'price': 50,
                'special_quantity': 5,
                'special_price': 150
            },
            'banana': {
                'price': 30,
                'special_quantity': 2,
                'special_price': 50
            },
            'cookie': {
                'price': 70,
                'special_quantity': 3,
                'special_price': 170
            },
            'duck': {
                'price': 250
            }
        }

    def test_normal_prices(self):
        expected_sum = 150
        shopping_basket = ['apple', 'banana', 'cookie']

        self.checkout = Checkout(self.checkout_test_config)
        checkout_sum = self.checkout.do_checkout(shopping_basket)

        self.assertEqual(checkout_sum, expected_sum)

    def test_empty_basket(self):
        expected_sum = 0
        shopping_basket = []

        self.checkout = Checkout(self.checkout_test_config)
        checkout_sum = self.checkout.do_checkout(shopping_basket)

        self.assertEqual(checkout_sum, expected_sum)

    def test_one_product_special_price(self):
        expected_sum = 50
        shopping_basket = ['banana', 'banana']

        self.checkout = Checkout(self.checkout_test_config)
        checkout_sum = self.checkout.do_checkout(shopping_basket)

        self.assertEqual(checkout_sum, expected_sum)

    def test_mixed_offers_prices_price(self):
        expected_sum = 620 #140 + 150 + 80 + 250
        shopping_basket = ['cookie', 'apple', 'banana', 'apple', 'banana', 'banana', 'apple', 'apple', 'cookie', 'apple', 'duck']

        self.checkout = Checkout(self.checkout_test_config)
        checkout_sum = self.checkout.do_checkout(shopping_basket)

        self.assertEqual(checkout_sum, expected_sum)

    def test_product_with_no_special_offer(self):
        expected_sum = 1000
        shopping_basket = ['duck', 'duck', 'duck', 'duck']

        self.checkout = Checkout(self.checkout_test_config)
        checkout_sum = self.checkout.do_checkout(shopping_basket)
        self.assertEqual(checkout_sum, expected_sum)

    def test_iterative_products_scanning_with_normal_offers(self):
        self.checkout = Checkout(self.checkout_test_config)

        self.checkout.scan('banana')
        self.assertEqual(self.checkout.total, 30)
        self.checkout.scan('duck')
        self.assertEqual(self.checkout.total, 280)
        self.checkout.scan('apple')
        self.assertEqual(self.checkout.total, 330)

    def test_iterative_products_scanning_with_special_offers(self):
        self.checkout = Checkout(self.checkout_test_config)

        self.checkout.scan('banana')
        self.assertEqual(self.checkout.total, 30)
        self.checkout.scan('banana')
        self.assertEqual(self.checkout.total, 50)

    def test_iterative_products_scanning_with_mixed_offers(self):
        self.checkout = Checkout(self.checkout_test_config)
        
        self.checkout.scan('banana')
        self.assertEqual(self.checkout.total, 30)
        self.checkout.scan('duck')
        self.assertEqual(self.checkout.total, 280)
        self.checkout.scan('apple')
        self.assertEqual(self.checkout.total, 330)
        self.checkout.scan('banana')
        self.assertEqual(self.checkout.total, 350)
        self.checkout.scan('cookie')
        self.assertEqual(self.checkout.total, 420)
        self.checkout.scan('apple')
        self.assertEqual(self.checkout.total, 470)
        self.checkout.scan('cookie')
        self.assertEqual(self.checkout.total, 540)
        self.checkout.scan('banana')
        self.assertEqual(self.checkout.total, 570)
        self.checkout.scan('cookie')
        self.assertEqual(self.checkout.total, 600)
